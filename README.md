# Slack chat bot
Previously this was a corporate slack bot I was developing for RnD department of one company.
Due to changes in the company this however was replaced with a service, and source codes were given into my disposal.

Solution requires sync with PostgreSQL database to handle permissions ( like starting deployment for DevOps ) and Jira server to represent tasks correctly.

By the time it was written Angular2-rc5 was latest version, it was restored later, but never updated to new stack. 

#### To get started:
* `npm install`
* setup db connection in `settings.json`
* `npm start`
